<?php

class Connection
{
    private static ?PDO $instance;

    /**
     * @return PDO|null
     */
    public static function getConnection(): ?PDO
    {
        if (self::$instance === null){
            self::$instance = new PDO('');
        }
        return self::$instance;
    }
}