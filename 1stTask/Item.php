<?php
include 'Connection.php';

final class Item
{

    private int $id;
    private string $name;
    private int $status;
    private bool $changed = false;

    /**
     * Item constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
        $this->init();
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->$name;
    }

    /**
     * @param string $name
     * @param $value
     */
    public function __set(string $name, $value)
    {
        switch ($name) {
            case 'name':
                if (is_string($value) && !empty($value)) {
                    $this->name = $value;
                    $this->changed = true;
                }
                break;
            case 'status':
                if (is_int($value)) {
                    $this->status = $value;
                    $this->changed = true;
                }
        }
    }

    /**
     *  fill name & status from data base
     */
    private function init()
    {
        $conn = Connection::getConnection();
        $nameAndStatusSelectionQuery = $conn->prepare(
            "SELECT name, status FROM objects WHERE id = :id"
        );
        $nameAndStatusSelectionQuery->execute([':id' => $this->id]);
        $result = $nameAndStatusSelectionQuery->fetchAll(PDO::FETCH_ASSOC);

        $this->name = $result['name'];
        $this->status = $result['status'];
    }

    /**
     *  object always exist in data base
     */
    public function save()
    {
        if ($this->changed) {
            $conn = Connection::getConnection();
            $nameAndStatusSelectionQuery = $conn->prepare(
                "UPDATE objects SET name = :name, status = :status WHERE id = :id"
            );
            $nameAndStatusSelectionQuery->execute([
                ':name' => $this->name,
                ':status' => $this->status,
                ':id' => $this->id
            ]);
        }
    }
}

